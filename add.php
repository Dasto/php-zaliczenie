<?php
	if(!isset($_SESSION['role']) and $_SESSION['role']!="admin"){
		header('Location: index.php');
		exit();
	}
?>
<form action="index.php" method="post">
			<fieldset>
				<legend>Add person:</legend>
				<table>
					<tbody>
						<tr>
							<td>First name:</td>
							<td>Last name:</td>
							<td>Savings:</td>
						</tr>
						<tr>
							<td><input type="text" name="firstname" placeholder="Enter first name" required></td>
							<td><input type="text" name="lastname" placeholder="Enter surname" required></td>
							<td><input id = "savingInput" type="number" name="savings" placeholder="Enter amount of savings" required></td>
						</tr>
					</tbody>
				</table>
				<input class="submit" name="add" type="submit" value="Add">
				<button type="button" class="cancelAdd" onclick="location.href = 'index.php'">Cancel</button>
				
	</fieldset>
</form>