<!DOCTYPE HTML>
<?php
		session_start();
		if (isset($_SESSION['licznik'])) {
			$_SESSION['licznik']++;
		}
		else {
			$_SESSION['licznik']=1;
		}
		if (isset($_GET['logout'])) {
			unset($_SESSION['zalogowany']);
			unset($_SESSION['role']);
		}
?>
<html>
<head>
	<title>PHP</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
	<header id="pageHeader">
	<?php
		if (isset($_SESSION['zalogowany'])) {
			echo '<h2>Witaj, '.$_SESSION['zalogowany'].'</h2>';
		}
		else{
			echo '<h2>Witaj, zaloguj się</h2>';
		}		
	?>
	</header>
	<article id="mainArticle">
		<?php
			if (isset($_SESSION['zalogowany'])){
				include 'result.php';
				
				if (isset($_SESSION['role']) and $_SESSION['role']=="admin"){
					if (isset ($_POST["showadd"])){
						include 'add.php';
					}
					else{
						echo 	'<form action="index.php" method="post">
									<input type="hidden" name="showadd" value="true" > 
									<button class = "submit" type="submit">Add</button>
								</form>';
					}
					if (isset($_POST["add"])) {
								$connect = mysqli_connect("localhost","root","") or
									die("Błąd połączenia z bazą danych");
								mysqli_select_db($connect, "zajecia");
								$firstname=$_POST["firstname"];
								$lastname=$_POST["lastname"];
								$savings=$_POST["savings"];
								$insert="INSERT INTO osoby (firstname,lastname,savings) 
									VALUES ('$firstname','$lastname',$savings);";
								$result = mysqli_query($connect,$insert) or die("Unenable to insert data");
								header('Location: index.php');
							 }
					else {'Nie udało się dodać danych.';}
					
				
					if (isset ($_POST["showedit"])){
						$_POST["editid"] = $_POST["showedit"];
						include 'edit.php';
					}
					if (isset ($_POST["edit"])){
								$connect = mysqli_connect("localhost","root","") or
									die("Błąd połączenia z bazą danych");
								mysqli_select_db($connect, "zajecia");
								if (isset($_POST["firstname"]) and isset($_POST["lastname"]) and isset($_POST["savings"]) and isset($_POST["id"])){
									
									$update="UPDATE osoby 
											SET firstname=\"{$_POST['firstname']}\",
											lastname=\"{$_POST['lastname']}\",
											savings=\"{$_POST['savings']}\"
											WHERE id={$_POST['id']}"; 
									$result = mysqli_query($connect,$update) or die("Unenable to update data");
								header('Location: index.php');
								}
									
					}
					else {'Nie udało się edytować danych.';}
				}
					
			}
			
		?>
		
	</article>
	<nav id="mainNav">
		<?php
			if (isset($_SESSION['zalogowany'])){
				include 'logout.php';
			}
			else{
				include 'loging.php';
			}
			if (isset($_SESSION['Error'])){
				echo $_SESSION['Error'];
			}
		?>
	</nav>
	<div id="siteClock">
		<canvas id="canvas" width="50" height="50">
		</canvas>
		<script type="text/javascript" src="clock.js" ></script>
		Dawid Stobiecki
	</div>
	<footer id="pageFooter">
	<?php
		if(isset($_COOKIE['wizyta'])) {
			$licznik=intval($_COOKIE['wizyta']);
			$licznik++;
			setcookie("wizyta","$licznik",time()+3600*24); 
			echo "Odświeżyłeś stronę $licznik razy w ostatnich 24 godzinach.";
		} 
		else {
			setcookie("wizyta","1",time()+3600*24);
			echo "Jesteś na tej stronie 1 raz w ostatnich 24 godzinach";
		}
		echo '<br>Licznik odświeżeń podczas sesji: ' . $_SESSION['licznik'];
	?>
	</footer>
</body>
</html>